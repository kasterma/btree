;;; memmen.asm
;;; Memory management for nodes and keyv for the B-tree
;;;
;;; Bart Kastermans, www.bartk.nl
;;; 
;;; The memory is managed as follows: both nodes and keyv struct
;;; are allocated on the heap.  The heap is divided in two parts,
;;; the used part (between heap and heapp) and the not yet used
;;; part.  Any node, resp. keyv, in the used part, when freed does not
;;; go back to the unused part, but gets added to nodel (a list
;;; of available nodes), keyvl (a list of available keyv)
;;; respectively.  So when a node(keyv) is needed, first check
;;; nodel(keyvl) and only when empty use additional space from
;;; the heap.

%include "btree.mac"
%include "strucd.mac"

section .text

%ifdef DEBUGMM
extern printf
%endif
        
;;; Get memory for a new node, put address in eax
;;; Returns 0 if there was no memory available
global newnode

;;; eax contains the address of a node, free it
global freenode

;;; Get memory for a new keyv, put address in eax
;;; Returns 0 if there was no memory available
global newkeyv        

;;; eax contains the address of a keyv, free it.
global freekeyv

;;; Set up the lists for keeping free nodes/keyvs
;;; The macros that act on these lists are heavily parametrized so that
;;; their register usage can easily be changed to avoid the registers
;;; used in B-tree functions.  The actual usage is set in the functions
;;; above (implemented after all macros are defined).
        
struc list
        .avail  resb    1       ; choice: keep doubles aligned/use less space
        .value  resd    1
        .next   resd    1
endstruc

%define listsize    1+4+4       ; size of a node in a list

        
;;; Find an unused list(node)
;;; put its address in the register given as argument
%macro  newlist 1
            mov     %1,listh    ; start at the top of the list heap
  %%check:
            cmp     byte [%1+list.avail],0x0
            je      %%found     ; found an available node
            add     %1,listsize ; move to next node on list heap
            jmp     %%check
  %%found:
            mov     byte [%1+list.avail],0x1
%endmacro

;;; Add value in %1 to the list in %4.
;;; Use %2 and %3 for temp storage.
%macro  addl  4
            mov     %3,[%4]
            newlist(%2)
            mov     [%2+list.value],%1
            mov     [%2+list.next],%3
            mov     [%4],%2
%endmacro

        
;;; From the list in %4, put value in head in %1, and free the head.
;;; Use %2 and %3 as temp storage.
%macro  popl  4
            mov     %2,[%4]
            mov     %1,[%2+list.value] ; get the value
            mov     %3,[%2+list.next]  ; get the tail
            mov     byte [(%2)+list.avail],0x0 ; free the node
            mov     [%4],%3
%endmacro

        
;;; Set equal flag if %2 is empty, use register %1
%macro  emptyl  2
            mov     %1,[%2]
            cmp     %1,0x0
%endmacro

;;; implementation of newnode
newnode:
        emptyl  eax,nodel
        je      .getfromheap    ; no nodes available on nodel
        popl    eax,ebx,ecx,nodel ; pop value from nodel into eax
        ;; ebx,ecx are used for temp storage
        ret
        
.getfromheap:
        cmp     dword [heapp],0x0       ; on first used initialize heapp to heap
        jne     .test2
        mov     dword [heapp],heap
        jmp     .get            ; certainly no full heap at this point
.test2: mov     eax,heap
        add     eax,HEAPSIZE
        cmp     eax,[heapp]
        jle     .heapfull
.get:   mov     eax,[heapp]
        add     dword [heapp],nodesize ;increase heappointer
        ret
.heapfull:
        mov     eax,0x0
        ret

;;; implementation of freenode
freenode:
        addl    eax,ebx,ecx,nodel ; add value onto nodel
        ret


;;; implementation of newkyv
newkeyv:
        emptyl  eax,keyvl
        je      .getfromheap    ; no keyv available on keyvl
        popl    eax,ebx,ecx,keyvl ;pop value from keyvl into eax
        ;; ebx,ecx are used for temp storage
        ret
        
.getfromheap:
        cmp     dword [heapp],0x0       ; on first used initialize heapp to heap
        jne     .test2
        mov     dword [heapp],heap
        jmp     .get            ; certainly no full heap at this point
.test2: mov     eax,heap
        add     eax,HEAPSIZE
        cmp     eax,[heapp]
        jle     .heapfull
.get:   mov     eax,[heapp]
        add     dword [heapp],keyvsize ; increase heappointer
        ret
.heapfull:
        mov     eax,0x0
        ret

;;; implementation of freekeyv
freekeyv:
        addl    eax,ebx,ecx,keyvl ; add value onto keyvl
        ret

%ifdef DEBUGMM
;;; Print the list in ebx
printl:
        push    ebx
        push    ebxm
        call    printf
     times 2 pop eax
        cmp     ebx,0x0
        je      .done

        mov     eax,[ebx+list.value]
        push    eax
        push    valm
        call    printf
     times 2 pop eax
        mov     ebx,[ebx+list.next]
        jmp     printl
.done:  ret

global printnl
printnl:
        mov     ebx,[nodel]
        call    printl
        ret

global printkvl
printkvl:
        mov     ebx,[keyvl]
        call    printl
        ret
        
;;; Print the length of the list in eax
countl: mov     ebx,0x0
.next:  cmp     eax,0x0
        je      .done
        add     ebx,0x1
        mov     eax,[eax+list.next]
        jmp     .next
.done:  push    ebx
        push    valm
        call    printf
     times 2 pop ebx
        ret        

global countnl
countnl:
        mov     eax,[nodel]
        call    countl
        ret

global countkvl
countkvl:
        mov     eax,[keyvl]
        call    countl
        ret
        
;;; Print values of heap, heapp, listh, nodel, keyvl
global printm
printm:
        push    heap
        push    heapm
        call    printf
     times 2 pop eax

        mov     eax,[heapp]
        push    eax
        push    heappm
        call    printf
     times 2 pop eax
        
        push    listh
        push    listhm
        call    printf
     times 2 pop eax
        
        mov     eax,[nodel]
        push    eax
        push    nodelm
        call    printf
     times 2 pop eax
        
        mov     eax,[keyvl]
        push    eax
        push    keyvlm
        call    printf
     times 2 pop eax
        
        ret
%endif                          ; DEBUGMM

section .bss
        
heap    resw    HEAPSIZE + nodesize ; the heap for nodes and keyv
;;; We add nodesize so that a full heap can be detected by heapp > heap + HEAPSIZE
heapp   resw    1               ; the heap pointer
;;; We allocate plenty room for the lists, otherwise freeing a node or keyv
;;; could fail due to not having memory available.
listh   resd    HEAPSIZE * (keyvsize / listsize)
nodel   resd    1               ; list of free nodes
keyvl   resd    1               ; list of free keyv

%ifdef DEBUGMM
section .data
 eaxm:   db      `eax: %x\n`,0x0
 ebxm:   db      `ebx: %x\n`,0x0
 valm:   db      `val: %x\n`,0x0
 valx:   db      `val: %x\n`,0x0
 heapm:  db      `heap: %x\n`,0x0
 heappm: db      `heapp: %x\n`,0x0
 listhm: db      `listh: %x\n`,0x0
 nodelm: db      `nodel: %x\n`,0x0
 keyvlm: db      `keyvl: %x\n`,0x0
%endif