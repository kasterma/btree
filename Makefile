# Makefile for the Btree in ASM
#
# Bart Kastermans, www.bartk.nl

NASMOPTIONS = -f elf -F stabs -g
GCCOPTIONS = -g
GLOBALINCLUDES = btree.mac strucd.mac

btree: btree.o
	gcc -g btree.o -0 btree

testbtree: btree.o memmen.o testbtree.o
	gcc $(GCCOPTIONS) btree.o memmen.o testbtree.o -o testbtree
	./testbtree

testmemmen: memmen.o testmemmen.o
	gcc -g memmen.o testmemmen.o -o testmemmen
	./testmemmen

testmemmen.o: testmemmen.asm  $(GLOBALINCLUDES)
	nasm $(NASMOPTIONS) testmemmen.asm

memmen.o: memmen.asm $(GLOBALINCLUDES)
	nasm $(NASMOPTIONS) memmen.asm

testbtree.o: testbtree.asm $(GLOBALINCLUDES)
	nasm $(NASMOPTIONS) testbtree.asm

btree.o: btree.asm $(GLOBALINCLUDES)
	nasm $(NASMOPTIONS) btree.asm

clean:
	rm -f memmen.o testmemmen.o testmemmen testbtree.o testbtree