;;; testbtree.asm
;;;
;;; Bart Kastermans, www.bartk.nl
;;;
;;; Testing the B-tree

%include "btree.mac"

%ifndef DEBUGBT
%error "ERROR: DEBUGBT not defined even though debugging btree.asm"
%endif
        
section .text

global main

main:   mov     eax,0x1
        mov     ebx,0x0
        int     0x80
        