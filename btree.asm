;;; btree.asm
;;; Implementing a B-tree (as in http://en.wikipedia.org/wiki/B-tree)
        
;;; Bart Kastermans, www.bartk.nl

%include "btree.mac"
%include "strucd.mac"
        
section .text

extern printf                   ; use function printf from stdlib

;;; These functions are from memmen.asm where the memory management is done.
extern newnode, freenode, newkeyv, freekeyv
        

;;; The B-tree interface

;;; Create a B-tree, its address is returned in eax
;;; 0 is returned in eax if there is no memory
global newbtree
        
;;; Insert value into the B-tree; stack should contain key, value, B-tree address
;;; eax contains 1 on success, 0 if there is no memory
;;; If key is already associated to a value in the B-tree, this value is updated
global insert

;;; Find a key in the B-tree; stack should contain key and B-tree address
;;; Value is returned in eax, ebx contains 1 if found, 0 if not found
global search

;;; Remove the value corresponding to a key in the B-tree; stack should contain
;;; the key to be removed and the B-tree address
global delete

;;; ;;;;;;;;;;;;;;;;;;;; IMPLEMENTATIONS ;;;;;;;;;;;;;;;;;;;;;;;;;;;
        
newbtree:
        call    newnode
        cmp     eax,0x0
        je      .return
        call    zeronode        ; If the newnode was previously used need to initialize
.return:
        ret

;;; Zero out the node whose address is in eax
zeronode:
        mov     ebx,eax
        add     ebx,nodesize+1   ; ebx now contains the first address not part of the node
.loop:
        mov     byte [eax],0x0
        add     eax,0x1
        cmp     eax,ebx
        jne     .loop

        ret



;;; Stack operations for the stack to use for insertion and deletion; for
;;; these operations we want to remember the path traveled though the tree.
;;; path push:
;;; Push %1 onto the path stack, use %2 for temp storage
%macro ppush 2
        mov     %2,pstack
        add     %2,[pstackp]
        add     word [pstackp],0x4
        mov     [%2],%1
%endmacro

%macro ppop 1
        mov     %1,pstack
        sub     word [pstackp],0x4
        add     %1,[pstackp]
        mov     %1,[%1]
%endmacro

;;; get an item from the stack without changing the stack
;;; %1: location to store it
;;; %2: how far up the stack to look (0 = top)
%macro pget 2
        mov     %1,-0x4
        add     %2,0x1
        mul     %1,%2           ; %2 contains offset from pointer
        sub     %2,0x1          ; restore count
        add     %1,[pstackp]
        add     %1,pstackp       ; %2 contains address
        mov     %1,[%1]         ;   The intel manuals don't exclude this, and seems alright
%endmacro

        ;;; When done with the stack execute clearstack to make it ready for next use
%define clearstack  mov word [pstackp],0x0

        
;;; Node stack; used to put fresh nodes on when inserting, otherwise
;;; might run out of memory in the middle of the operation and can't really
;;; recover the original tree
;;; Use %1 for temp storage (different from eax, since eax should be preserved)
;;; %2 (also different from eax) contains the address of the new node
%macro putnodest 2
        mov     %1,eax          ; save eax

        mov     %2,nodestack    ; compute where to store next value
        add     %2,[nodestackp]
        add     word [nodestackp],0x4 ; update stack pointer

        call    newnode
        mov     [%2],eax        ; store the next node
        mov     %2,eax
        mov     eax,%1
%endmacro


%macro popnodest 1
        sub     word [nodestackp],0x4
        mov     %1,nodestack
        add     %1,[nodestackp]
        mov     %1,[%1]
%endmacro
        
;;; Several of the functions below take their inputs on the stack, these
;;; macros are for easy access.
%define arg1    [esp+4]
%define arg2    [esp+8]
%define arg3    [esp+12]

;;; Get address of separator %1 of node with address in %2 in register %3
%macro getsepaddress 3
        mov     %3,%1
        sub     %3,0x1
        mul     %3,0x8
        add     %3,0x2          ; computed 2+(8*(i-1))
        ;; computed 2+(8*(i-1)), this is the offset of the i-th separator from the
        ;; start of the node
        add     %3,%2
        ;; Now %3 contains the address of the i-th separator
%endmacro

;;; Change %1 from the separator number to its address, for node with address in %2
%macro tosepaddress 2
        sub     %1,0x1
                         mul     %1,0x8
        add     %1,0x2
        add     %1,%2
%endmacro
        
;;; Put val (key) of separator %1 of node with address in %2 in register %3
%macro getsepval 3
        getsepaddress %1,%2,%3
        mov     %3,[%3+keyv.value]
%endmacro
        
%macro getsepkey 3
        getsepaddress %1,%2,%3
        mov     %3,[%3+keyv.key]
%endmacro

;;; Set val of separator %1 of node with address in %2 to the value in register %3
;;; Use register %4 for temp storage
%macro setsepval 4
        getsepaddress %1,%2,%4
        mov     [%4+keyv.value],%3
%endmacro

;;; Checks if the node with address in eax is full (no empty separators)
;;; Sets equal flag if not empty
;;; %1 is used for temp storage
%macro eaxnodefull 1
        mov     %1,nochild-1
        tosepaddress %1,eax
        cmp     word [%1],0x0
%endmacro
        
;;; arg3 = key
;;; arg2 = value
;;; arg1 = B-tree address
;;; eax contains 1 on success, 0 if there is no memory
insert: call    newkeyv
        cmp     eax,0x0
        je      .nomem          ; no memory available
        mov     ebx,arg3
        mov     ecx,arg2
        mov     [eax+keyv.key],ebx
        mov     [eax+keyv.value],ecx
        mov     [keyvtoadd],eax
        ;; Now ebx contains the key value, and [keyvtoadd] the
        ;; address of the keyv to add to the Btree

        mov     eax,arg1        ; eax will contain the node under investigation

.loop:                          ; loop to run to the correct leaf of the tree
        mov     ecx,0x1

.seploop:                       ; loop running through the separators; decide where to go
        cmp     ecx,nochild
        je      .tochildafter   ; Have finished looking at the separators

        cmp     word [ecx],0x0
        je      .tochildbefore  ; The next separator does not exist
                                ; means that the child before is the rightmost tree

        getsepkey ecx,eax,edx
        cmp     ebx,edx
        je      .exactmatch
        jl      .tochildbefore

        add     ecx,0x1         ; To next separator
        jmp     .seploop

.tochildbefore:
        ;; Note that because non-leaf nodes only have keyv that have been
        ;; pushed up as separators, we know that if this child node is NULL
        ;; we are at the appropriate leaf node
        
        ppush   eax,edx         ; push the address (in eax) to the path stack
        ;; edx was clobbered here

        tosepaddress ecx,eax    ; change ecx from separator number to its address
        sub     ecx,0x4
        jmp     .tochildcommon

.tochildafter:
        ;; Note that because non-leaf nodes only have keyv that have been
        ;; pushed up as separators, we know that if this child node is NULL
        ;; we are at the appropriate leaf node

        ppush   eax,edx         ; push the address (in eax) to the path stack
        ;; edx was clobbered here

        tosepaddress ecx,eax    ; change ecx from separator number to its address        
        add     ecx,0x4         ; compute address of child node

        ;; !!!!!!!!!!!! fall through to .tochildcommon !!!!!!!!!!!!!!

.tochildcommon:
        cmp     word [ecx],0x0
        je      .reachedleaf
        ;; Set up for the next loop: eax address of node
        ;; ebx still contains the key we are trying to place
        mov     eax,ecx         ; move address of next node into eax
        
        jmp     .loop

.reachedleaf:
        ;; now we are at a leaf node where the keyv should go, and the path stack
        ;; contains the path to this leaf (including this leaf itself).
        ;; eax contains the address of this leaf node
        ;; ebx contains the key of the keyv to be inserted
        eaxnodefull ecx
        je      .justinsert     ; This leaf node has room, so just insert

        ;; The next part of the program checks how many nodes need to be split
        ;; and allocates the memory needed for the splitting.
        mov     ecx,0x0         ; counter of how high on the path stack we are
.getspaceloop:
        putnodest   edx,esi     ; Put a node on the stack
        cmp     esi,0x0         ; Check if there was memory available for the node
        je      .nospace          ; Not enough memory available
        add     ecx,0x1         ; increase counter
        pget    eax,ecx         ; get address from the path stack
        eaxnodefull edx         ; check if found a node with room
        jne     .getspaceloop   ; if not, loop
        
        mov     ebp,0x0         ; flag for on which side of the median the new key falls
.splitting:                     ; This is the part of the program where
                                ; the real work is done (nodes split and organized)
        ppop    eax
        popnodest   ecx
        ;; Fresh node address in ecx
        ;; Key of node to be added in ebx
        ;; Address of leaf node in eax
        ;; Must spread keyv in node in eax and [keyv] over node in eax and node in ecx
        mov     edx,0x1
        mov     esi,nochild-1
.splittingloop:
        getsepkey edx,eax,edi   ; edi contains key for separator edx in eax
        cmp     ebx,edi
        jg      .nota
        mov     ebp,0x1
.nota:
        getsepkey esi,eax,edi
        cmp     ebx,edi
        jl      .notb
        mov     ebp,0x2
.notb:
        add     edx,0x1
        sub     esi,0x1
        cmp     edx,esi
        je      .equal
        jl      .overlapped
        jg      .splittingloop

.equal:
;;;-------------------- NOT DONE   
.overlapped:
;;; -------------------- NOT DONE
;;; -----------  AFTER THIS WORK UP THE TREE -----------------
        jmp     .finish

.nospace:                       ; Run out of space in the .getspaceloop
        cmp     word [nodestackp],0x0 ; Check if node stack is empty
        je      .nomem          ; Node stack empty, return with right error condition
        popnodest eax
        call    freenode
        jmp     .nospace

        
.justinsert:
        mov     ecx,0x1
.sloop:                         ; search for location where the new keyv should go
        getsepaddress ecx,eax,edx
        cmp     word [edx],0x0       ; equal if separator does not exist
        je      .inserthere     ; the key to be added is larger than all in the node
        
        getsepkey ecx,eax,edx
        cmp     ebx,edx
        jg      .move
        add     ecx,0x1
        jmp     .sloop
        
.move:                          ; move the larger keyv up (note: we know there are such)
        mov     edx,ecx
.toend:                         ; scan to the end of the separators
        add     edx,0x1
        getsepaddress edx,eax,esi
        cmp     word [esi],0x0
        je      .foundlargest
        jmp     .toend
        
.foundlargest:                  ; move up until at index ecx
        mov     edi,esi
.loopfl:
        sub     edi,nodesize+keyvsize ; edi points to keyv before esi
        mov     [esi],[edi]
        sub     edx,0x1
        mov     esi,edi
        jne     .loopfl

        ;; !!!!!!!!!!!!!! fall through on purpose !!!!!!!!!!!!!!!

        ;; need to insert at the separator with address in edi

.insertatsep:
        mov     edx,edi         ; contains address of last moved keyv

        ;; !!!!!!!!!!!!!! fall through on purpose !!!!!!!!!!!!!!!!
        
.inserthere:
        mov     eax,[keyvtoadd]
        mov     [edx],eax
        jmp     .finish
        
        
.exactmatch:                    ; found an exact match for key, update the keyv
        mov     edx,arg2        ; put value in edx
        setsepval ecx,eax,ebx,edx ; updated the value of the node

        mov     eax,[keyvtoadd]
        call    freekeyv
        
        jmp     .finish
        
.nomem: mov     eax,0x0         ; return reporting failure
        clearstack
        ret
        
.finish:                        ; return reporting success
        mov     eax,0x1
        clearstack
        ret
        
        
        
;;; key is arg2
;;; B-tree address is arg1
search: mov     eax,arg1        ; get address of B-tree from the stack
        ;; this address is the address of the root node of the tree
        mov     ebx,arg2        ; get key from the stack
        call    search_h
        ret

;;; Search function that expects address of root of B-tree in eax
;;; and the key to search for in ebx
search_h:
        mov     ecx,0x1
.loop:
        cmp     ecx,nochild
        je      .tochild        ; have passed all separators, so move to last child

        getsepaddress   ecx,eax,edx
        cmp     word [edx],0x0       ; check if separator exists
        je      .tochild

        mov     edx,[edx+keyv.key] ; get the key of the separator
        
        cmp     ebx,edx         ; compare key to searched for value
        jl      .tochild
        je      .keyinsep

        add     ecx,0x1         ; move to next separator
        jmp     .loop
        
.keyinsep:                      ; key found in separator, get its value
        getsepval   ecx,eax,edx
        mov     eax,edx
        mov     ebx,0x1         ; report success
        ret
        
.tochild:       ; move to child just before separator ecx
        sub     ecx,0x4         ; compute address of child node
        cmp     word [ecx],0x0       ; check if child exists
        je      .nochild        ; nochils--key not found in Btree

        mov     eax,[ecx]       ; move to child, ebx still contains the key to search for
        jmp     search_h

.nochild:
        mov     ebx,0x0         ; report failure
        ret
        
        
%ifdef DEBUGBT
;;; Print the B-tree with address in eax a string representation
global printbt

%endif

        
section .bss
keyvtoadd:      resw    1
pstack:         resw    STACKSIZE ; stack for the path through the tree
pstackp:        resw    1
nodestack:      resw    STACKSIZE ; stack for allocating nodes
nodestackp:     resw    1