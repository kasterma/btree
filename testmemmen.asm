;;; testmemmen.asm
;;;
;;; Bart Kastermans, www.bartk.nl
;;;
;;; Testing memmen.asm functions.

%include "btree.mac"
%include "strucd.mac"

%ifndef DEBUGMM
%error "ERROR: DEBUGMM not defined even though debugging memmen"
%endif
        
section .text

extern newnode, freenode, newkeyv, freekeyv
extern printf                   ; from standard libraries
%ifdef DEBUGMM
extern printm, printnl, printkvl, countnl, countkvl
%endif
        
global  main

main:   call    printm

        call    newnode
        mov     [node1],eax

        push    eax
        push    eaxm
        call    printf
     times 2 pop eax
        
        call    printm

        call    newnode
        mov     [node2],eax
        push    eax
        push    eaxm
        call    printf
     times 2 pop eax

        call    printm

        call    newnode
        mov     [node3],eax
        push    eax
        push    eaxm
        call    printf
     times 2 pop eax

        call    printm

        push    threen
        call    printf
        pop     eax             ; three nodes on the heap


        mov     eax,[node2]
        call    freenode

        call    printm
        call    printnl
        call    countnl
        call    countkvl
        
        push    thol
        call    printf
        pop     eax             ; Two on the heap, one on the node list

        mov     eax,[node1]
        call    freenode

        call    printm
        call    printnl
        call    countnl
        call    countkvl
                
        push    ohtl
        call    printf
        pop     eax             ; One on the heap, two on the node list

        call    newnode

        call    printm
        call    printnl
        call    countnl
        call    countkvl
        
        push    thol
        call    printf
        pop     eax             ; Two on the heap, one on the node list

        call    newnode

        call    printm
        call    printnl
        call    countnl
        call    countkvl
        
        push    threen
        call    printf
        pop     eax             ; Three nodes on the heap

        call    newnode
        mov     [node1],eax
        
        call    printm
        call    printnl
        call    countnl
        call    countkvl
        
        push    fourn
        call    printf
        pop     eax             ; Four nodes on the heap


        call    newkeyv
        mov     [keyv1],eax
        
        call    printm
        call    printnl
        call    printkvl
        call    countnl
        call    countkvl
        
        push    onek
        call    printf
        pop     eax             ; One keyv on the heap

        call    newkeyv
        mov     [keyv2],eax
        
        call    printm
        call    printnl
        call    printkvl
        call    countnl
        call    countkvl
        
        push    twok
        call    printf
        pop     eax             ; Two keyv on the heap


        call    newkeyv
        mov     [keyv3],eax
        
        call    printm
        call    printnl
        call    printkvl
        call    countnl
        call    countkvl
        
        push    threek
        call    printf
        pop     eax             ; Three keyv on the heap

        mov     [keyv1],eax
        call    freekeyv
        
        call    printm
        call    printnl
        call    printkvl
        call    countnl
        call    countkvl
        
        push    tkhokl
        call    printf
        pop     eax             ; Two keyv on heap, one on list

        call    newkeyv
        
        call    printm
        call    printnl
        call    printkvl
        call    countnl
        call    countkvl
        
        push    threek
        call    printf
        pop     eax             ; Three keyv on the heap


        mov     ecx,0x0
        
        jmp     fillwkeyv       ; Comment out to fill with nodes
        
again:  call    newnode
        cmp     eax,0x0
        je      heapfilled
        add     ecx,0x1
        jmp     again

fillwkeyv:      
againkv:call    newkeyv
        cmp     eax,0x0
        je      heapfilled
        add     ecx,0x1
        jmp     againkv

heapfilled:
        push    ecx
        push    ecxm
        call    printf
     times 2 pop eax

        
return: mov     eax,0x1
        mov     ebx,0x0
        int     0x80

section .data
fourn:  db    `        Now have four nodes on the heap.\n`,0x0
threen: db    `        Now have three nodes on the heap.\n`,0x0
thol:   db    `        Two on the heap, one on the node list.\n`,0x0
ohtl:   db    `        One on the heap, two on the node list.\n`,0x0
onek:   db    `        One keyv on the heap.\n`,0x0
twok:   db    `        Two keyv on the heap.\n`,0x0
threek: db    `        Three keyv on the heap.\n`,0x0
tkhokl: db    `        Two keyv on the heap, one on the list.\n`,0x0
eaxm:   db      `eax: %x\n`,0x0
ecxm:   db      `ecx: %x\n`,0x0
        
section .bss

node1:  resw    1
node2:  resw    1
node3:  resw    1
keyv1:  resw    1
keyv2:  resw    1
keyv3:  resw    1